import { useUser } from "../Context/UserContext"
import {Navigate} from 'react-router-dom'

// Uses props to prevent the user to access the
// translation and profile page if they are not logged in
const withAuth = Component => props => {
    const {user} = useUser()
    if (user !== null) {
        return <Component {...props} />
    }else{
        return <Navigate to="/" />
    }
}
export default withAuth