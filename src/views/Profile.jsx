import { useEffect } from "react";
import { userById } from "../Api/user";
import { STORAGE_KEY_USER } from "../const/StorageKeys";
import { useUser } from "../Context/UserContext";
import { storageSave } from "../Utils/Storage";
import ProfileActions from "../Components/Profile/ProfileAction";
import ProfileHeader from "../Components/Profile/ProfileHeader";
import ProfileTranslationHistory from "../Components/Profile/ProfileTranslationHistory";

import withAuth from "../hoc/withAuth";

function Profile() {
  const { user, setUser } = useUser();

  // Checks the id of the user and saves the record and state to the storage
  useEffect(() => {
    const findUser = async () => {
      const [error, latestUser] = await userById(user.id);
      if (error === null) {
        storageSave(STORAGE_KEY_USER, latestUser);
        setUser(latestUser);
      }
    };

    findUser();
  }, [setUser, user.id]);

  // Display the profile page with corresponding components
  return (
    <>
      
      <ProfileHeader username={user.username} />
      <ProfileActions />
      <ProfileTranslationHistory translations={user.translations} />
    </>
  );
}
export default withAuth(Profile);
