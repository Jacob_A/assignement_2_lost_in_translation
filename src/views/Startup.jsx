import StartupForm from "../Components/Startup/StartupForm";
// Wrapper code to display the startup page
const Startup = () => {
  return (
    <>
      <StartupForm />
    </>
  );
};

export default Startup;
