import TranslationForm from "../Components/Translation/TranslationForm";
import withAuth from "../hoc/withAuth";
// Wrapper code to display the translation page
const Translation = () => {
  return (
    <>
      <TranslationForm />
    </>
  );
};

export default withAuth(Translation);
