// Wrapper to give access to information between components (context api)
// prevents prop-drilling
import UserProvider from "./UserContext";

const AppContext = ({ children }) => {
  return <UserProvider>{children}</UserProvider>;
};
export default AppContext;
