const validateKey = (key) => {
  if (!key || typeof key !== "string") {
    throw new Error("Storage: Invalid storage key provided");
  }
};

// Saves the record of the user to the local storage
export const storageSave = (key, value) => {
  validateKey(key);
  if (!value) {
    throw new Error("StorageSave: No value provided for " + key);
  }

  localStorage.setItem(key, JSON.stringify(value));
};

// Reads the record of the user by finding the key
export const storageRead = (key) => {
  validateKey(key);

  const data = localStorage.getItem(key);

  if (data) {
    return JSON.parse(data);
  }

  return null;
};

// Delete record of the user using the key
export const storageDelete = (key) => {
  validateKey(key);
  localStorage.removeItem(key);
};
