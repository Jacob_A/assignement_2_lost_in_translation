import { useState } from "react";
import { useForm } from "react-hook-form";
import { addTranslation } from "../../Api/translation";
import { STORAGE_KEY_USER } from "../../const/StorageKeys";
import { useUser } from "../../Context/UserContext";
import { storageSave } from "../../Utils/Storage";

// Function to import all images from specified location
// returns the imported images
function importAll(r) {
  let images = {};
  r.keys().map((item, index) => {
    images[item.replace("./", "")] = r(item);
  });
  return images;
}

const TranslationForm = () => {
  const {
    register,
    handleSubmit
  } = useForm();
  const [handSign, setHandSigns] = useState([]);
  // Imports the hand sign images
  const images = importAll(
    require.context(
      "../../../public/individual_signs_img",
      false,
      /\.(png|jpe?g|svg)$/
    )
  );
  const { user, setUser } = useUser();

  // Handles the action when the translation button is pressed
  const onSubmit = async (translationOrder) => {
    setHandSigns(Array.from(translationOrder.translationOrder));

    const [error, updatedResult] = await addTranslation(
      user,
      translationOrder.translationOrder
    );
    if(error !== null)
    {
        return null;
    }

    // Update the state of the local storage
    storageSave(STORAGE_KEY_USER, updatedResult);
    setUser(updatedResult);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <fieldset className="title-translation">
        <label htmlFor="order-notes">Translate this: </label>
        <input
          type="text"
          {...register("translationOrder")}
          placeholder="Hello"
        />
             <button className="button" type="submit">Translate</button>
        <br/><br/><br/><br/>
        <div className="translation-card" >
          <div className="sign-box">
            {handSign.map((char, i) => {
              if (char === " ") {
                return <div key={i} className="space"></div>;
              }
              return (
                <img
                  key={i}
                  src={images[char.toLowerCase() + ".png"]}
                  alt="Hand sign"
                  style={{ width: "50px", height: "50px" }}
                ></img>
              );
            })}
          </div>
        </div>
      </fieldset>

 
    </form>
  );
};

export default TranslationForm;
