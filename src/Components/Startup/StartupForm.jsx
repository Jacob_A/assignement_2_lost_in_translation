import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { loginUser } from "../../Api/user";
import { storageSave } from "../../Utils/Storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../Context/UserContext";
import { STORAGE_KEY_USER } from "../../const/StorageKeys";
import logo from '../../logo/Logo.png'


// Config for the username, the username need to be large than 3 characters
const usernameConfig = {
  required: true,
  minLength: 3,
};

const StartupForm = () => {

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  // Sets active user and redirect
  const { user, setUser } = useUser();
  const navigate = useNavigate();

  // Sets start-state
  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);

  // if the user is already logged in, redirected to the translation page
  useEffect(() => {
    if (user !== null) {
      navigate("translation");
    }
  }, [user, navigate]); //[] means it only run once

  const onSubmit = async ({ username }) => {
    // Sets loading, state to make sure the user can not "spam" the button
    // faster than the api can respond
    setLoading(true);
    // Calls the login function from component
    const [error, userResponse] = await loginUser(username);
    if (error !== null) {
      setApiError(error);
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }
    setLoading(false);
  };

  const errorMsg = (() => {
    if (!errors.username) {
      return null;
    }
    if (errors.username.type === "required") {
      return <span>Username is required</span>;
    }

    if (errors.username.type === "minLength") {
      return <span>Username is too short (min. 3)</span>;
    }
  })();
const arrow = '->'
  return (
    <>
    <div className="title-startup">
    <div>
        <br/>
        <img src={logo} alt="robot logo" style={{width:'90px', height:'100px', float:"left" ,paddingTop:'50px' ,paddingLeft:'20%', marginRight:'0px'}}></img>
      
      <h2 style={{textAlign:'center' }}>Lost in translation</h2>
      
      <p style={{textAlign:'center'}}>Get started</p>
      </div>
      <div className="login-card">
      <form onSubmit={handleSubmit(onSubmit)}>
        <fieldset className="login">
          <label>Username</label>
          <input
            type="text"
            placeholder="Eric"
            {...register("username", usernameConfig)}
          ></input>
          {errorMsg}
          <button type="submit" className="login-button" disabled={loading}>
          Login
        </button>
        </fieldset>
        
        {loading && <p>Logging in...</p>}
        {apiError && <p>{apiError}</p>}
      </form>
      </div>
      </div>
    </>
  );
};

export default StartupForm;
