import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";

const ProfileTranslationHistory = ({ translations }) => {
  // Checks if the currently number of fetched translations of the user is larger than 10
  // if so, slice the array of translations from the "array size - 10", giving us the
  // index of the first item of the 10 latest items. Display the reverse order of these so the latest item
  // is displayed first

  let translationList;
  if (translations.length > 10) {
    let newTranslations = translations
      .slice(translations.length - 10)
      .reverse();
    translationList = newTranslations.map((translation, index) => (
      <ProfileTranslationHistoryItem key={index} translation={translation} />
    ));
  } else {
    translationList = translations
      .map((translation, index) => (
        <ProfileTranslationHistoryItem translation={translation} key={index} />
      ))
      .reverse();
  }

  return (
    <section>
      <h4>Your translation history</h4>
      {translationList.length === 0 && (
        <p> No translations has been made yet, go translate some words! :-)</p>
      )}
      <ul>{translationList}</ul>
    </section>
  );
};
export default ProfileTranslationHistory;
