// Adds welcome message specified by the username
const ProfileHeader = ({ username }) => {
    return (
        <h4 style={{textAlign:'center'}}>Hello, welcome back {username}</h4>
    )
};
export default ProfileHeader;
