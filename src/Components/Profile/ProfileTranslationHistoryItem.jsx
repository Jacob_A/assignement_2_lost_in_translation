// Component to display each item in the array of translations
const ProfileTranslationHistoryItem = ({ translation }) => {
  return <li>{translation}</li>;
};
export default ProfileTranslationHistoryItem;
