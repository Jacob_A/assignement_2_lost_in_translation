import { clearTranslationHistory } from "../../Api/translation";
import { STORAGE_KEY_USER } from "../../const/StorageKeys";
import { useUser } from "../../Context/UserContext";
import { storageDelete, storageSave } from "../../Utils/Storage";

// Fetches the user-context, knowing which user is current logged in
const ProfileActions = ({ logout }) => {
  const { user, setUser } = useUser();

  // Handle the deletion of the local storage if the user logs out
  const handleLogoutClick = () => {
    if (window.confirm("Are you sure")) {
      storageDelete(STORAGE_KEY_USER);
      setUser(null);
    }
  };

  // Clears history, calls the clear history function from component
  const handleClearHistoryClick = async () => {
    if (!window.confirm("Are you sure\nThis can not be undone!")) {
      return;
    }
    const [clearError] = await clearTranslationHistory(user.id);
    if (clearError !== null) {
      return;
    }

    // Updates the user data for the local storage, clears current info
    // "Syncs" the api status with the local storage
    
    // NOTE: As the api resets after some time to a normal state, if the app is going
    // for too long, the states will not be sync'd, a way to solve this could be using the 
    // session storage. But as this app will not be used for an extended period of time, the 
    // choice was made to keep using local storage for ease. 

    const updateUser = {
      ...user,
      translations: [],
    };
    // Update states after "clearing" the user information for translation
    storageSave(STORAGE_KEY_USER,updateUser);
    setUser(updateUser);
    
  };
  return (
    <ul>
      <li>
        <button className="button" style={{margin:'15px'}} onClick={handleLogoutClick}>Logout</button>
      </li>
      <li>
        <button className="button" onClick={handleClearHistoryClick}>Clear History</button>
      </li>
    </ul>
  );
};
export default ProfileActions;
