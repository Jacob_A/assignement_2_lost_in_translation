import {NavLink} from 'react-router-dom'
import { useUser } from '../../Context/UserContext'


// Nav-bar, placed above the app routing to exist for all pages
const Navbar = () =>{
  const { user, setUser } = useUser()

  if (user !== null) {
    return(
     
      <nav className='navbar'>
        <div style={{display:'flex'}}>
        <ul>Lost in translations</ul>
        <div style={{display:'flex', justifyContent:'right'}}>
        <ul><NavLink to="/translation">Translation</NavLink></ul>    
        <ul><NavLink to="/profile">Profile: {user.username}</NavLink></ul>
        
        </div>
        </div>
      </nav>

      
  )
  }else
    return(
        <nav className='navbar'>
          <ul className='welcome'>Welcome to the sign language translation page!!</ul>
        </nav>
    )
}//Add if user is online then they wont be able to se the other links.


export default Navbar