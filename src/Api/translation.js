import { createHeaders } from "./index";

// Adds translation to the api, appends onto the current
// existing list of translations

export const addTranslation = async (user, translation) => {
  // Stores the api url into a variable from the env
  const apiUrl = process.env.REACT_APP_API_URL;
  // Try-catch together with async for asynchronous reading
  try {
    // Fetches the current user's information and patches
    // a record into the api, adding the information
    const response = await fetch(`${apiUrl}/${user.id}`, {
      method: "PATCH",
      headers: createHeaders(),
      body: JSON.stringify({
        translations: [...user.translations, translation],
      }),
    });
    // if a response is not given/not good, throw error
    if (!response.ok) {
      throw new Error("could not update the translation");
    }

    // Return the response as the result given by the api
    const result = await response.json();
    return [null, result];
  } catch (error) {
    // Returns the error message if error was thrown
    return [error.message, null];
  }
};

/* Clears the translations from the api

 An assumptions was made that the an acceptable way to "clear" the history 
 was to supply the api with a empty array. 

 This is of course not best practice as it violates 
 the rule of not deleting records in the database, but for the sake of ease, 
 as well as the given description in the assignment description,
 this approach was taken*/

export const clearTranslationHistory = async (userId) => {
  const apiUrl = process.env.REACT_APP_API_URL;
  // Try-catch together with async for asynchronous reading
  try {
    // Fetches the current user's information and patches
    // a record into the api, "replacing" the information
    const response = await fetch(`${apiUrl}/${userId}`, {
      method: "PATCH",
      headers: createHeaders(),
      body: JSON.stringify({
        translations: [],
      }),
    });
    // if a response is not given/not good, throw error
    if (!response.ok) {
      throw new Error("Could not update the order");
    }

    // Return the response as the result given by the api
    const result = await response.json();
    return [null, result];
  } catch (error) {
    // Returns the error message if error was thrown
    return [error.message, null];
  }
};
