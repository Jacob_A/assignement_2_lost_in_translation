const apiKey = process.env.REACT_APP_API_KEY

// Creates the content type and supplies the api-key to use for RESTApi requests.
export const createHeaders = () =>{
    return{
        'Content-Type':'application/json',
        'x-api-key': apiKey
    }
}