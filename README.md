# Assignment 2, Lost in Translation 

# Install
To run the project;
```
git clone https://gitlab.com/Jacob_A/assignement_2_lost_in_translation
cd assignment_2_lost_in_translation
npm install 
```
This will clone the repo to the folder and install the dependencies needed.

# Usage
To run the app, use:

```
npm start
```

This will start the app on your localhost with port 3000.

To connect to the REST API, an .env file has to be created and place on the same level as the package.json. This file has to contain the following: 

```
REACT_APP_API_KEY=<Add key here>
REACT_APP_API_URL=<Add url here>
```

# Contributors
[Jacob Andersson (@Jacob_A)](@Jacob_A)

[Robin Axelstorm (@Robinax)](@Robinax)

# Contributing
PRs are not accepted for this project.
